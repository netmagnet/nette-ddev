# DDEV module

## Configuration

```
extensions:
    ddev: Slts\Ddev\DI\DdevExtension

# fixRouting: true is default, whole section can be omited
ddev:
    fixRouting: true
```

## Fix routing

In case you can't use this extension, you can always do it manually.

It simulates this:
```
http:
    proxy:
        - THIS VALUE MUST BE $_SERVER['REMOTE_ADDR']
```
