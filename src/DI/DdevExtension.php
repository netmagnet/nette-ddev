<?php

namespace Slts\Ddev\DI;

use Nette\DI\Definitions\ServiceDefinition;
use Nette\DI\Helpers;
use Nette\DI\MissingServiceException;
use Nette\DI\CompilerExtension;


class DdevExtension extends CompilerExtension
{
    /**
     * @var array $defaults
     */
    private $defaults = [
        'fixRouting' => true,
    ];

    public function beforeCompile()
    {
        $builder = $this->getContainerBuilder();
        $expandedParams = Helpers::expand($this->defaults, $builder->parameters);
        $this->validateConfig($expandedParams);
        $config = $this->getConfig();

        $consoleMode = $builder->parameters['consoleMode'] ?? (PHP_SAPI === 'cli');

        if (!$consoleMode && $config['fixRouting']) {
            try {
                /** @var ServiceDefinition $requestFactory */
                $requestFactory = $builder->getDefinition('http.requestFactory');
                foreach ($requestFactory->getSetup() as $setup) {
                    if ((is_string($setup->entity) && $setup->entity === 'setProxy') || (is_array($setup->entity) && $setup->entity[1] === 'setProxy')) {
                        $setup->arguments[0][] = $_SERVER['REMOTE_ADDR'];
                    }
                }

            } catch (MissingServiceException $e) {
            }
        }
    }
}
